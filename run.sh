source config.env

# Get comma separated list of input source locations on local machine
# and convert it into docker container file locations
# IFS is special field separator variable
IFS=","
read -ra arr <<< "$SOURCE_PATH"

# Need to compose space separated list of docker internal volume locations that get
# fed into the main BIL OME-Zarr converter program
# Also compose a mapping from the tif folder locations to those docker internal volume locations
# that are used when actually launching the docker container
DOCKER_SOURCE_INPUT=""
INPUT_VOLUME_MNTS=""
index=0
for local_location in "${arr[@]}"
do
    docker_location="/data/input${index}"
    local_location=`echo $local_location | awk '{$1=$1};1'`
    volume_mount_opt="-v ${local_location}:${docker_location}"

    if [ $index -eq 0 ]; then
        DOCKER_SOURCE_INPUT=$docker_location
        INPUT_VOLUME_MNTS=$volume_mount_opt
    else
        DOCKER_SOURCE_INPUT="${DOCKER_SOURCE_INPUT} ${docker_location}"
        INPUT_VOLUME_MNTS="${INPUT_VOLUME_MNTS} ${volume_mount_opt}"
    fi
    index=$((index+1))
done

# We need to dynamically compose the docker run command because we don't
# know how many -v volume mount options to add until runtime
docker login -u bilconv-docker -p W5PbZH4Js2AHftRowEk3 registry.gitlab.com
RUN_CMD="docker run -it $INPUT_VOLUME_MNTS -v ${OUTPUT_PATH}:/data/output.omehans -e CONV_ARGS=\"${CONV_ARGS}\" -e DOCKER_IN_LOCATIONS=\"${DOCKER_SOURCE_INPUT}\" registry.gitlab.com/cgs-neurons/bilconv"
eval $RUN_CMD