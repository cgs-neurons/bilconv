@ECHO off
SETLOCAL EnableDelayedExpansion
SET varname=
SET varvalue=
FOR /F "delims== eol=# tokens=1,*" %%i IN ('type config.env') DO (
  IF "%%j"=="" (
    SET t=%%i
    SET q=!t:\=!
    CALL SET varvalue=!varvalue! !q!
    SET !varname!=!varvalue!
  ) else (
    SET t=%%j
    SET varname=%%i
    if !varname!==SOURCE_PATH (
      SET varvalue=!t!
    ) else if !varname!==OUTPUT_PATH! (
      SET varvalue=!t!
    ) else (
      SET varvalue=!t:\=!
    )
    SET !varname!=!varvalue!
  )
)
docker login -u bilconv-docker -p W5PbZH4Js2AHftRowEk3 registry.gitlab.com
docker run -it ^
  -v %SOURCE_PATH%:/data/input ^
  -v %OUTPUT_PATH%:/data/output.omehans ^
  -e CONV_ARGS=%CONV_ARGS% ^
  registry.gitlab.com/cgs-neurons/bilconv
ENDLOCAL