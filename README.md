# Docker image for stack_to_multiscale_ngff

## Purpose

The goal is to simplify the use of stack_to_multiscale_ngff conversion tool. Avoids the need to:
* Install python
* Setup virtual environment
* Pass configuration parameters on the command line

## Prerequisites

* Docker
* Linux (or WSL, etc)

## Usage

### Clone this repository

__Note__: Only the `config.env` and `run.sh` files are actually needed.

```
Download and unzip the this repository:
https://gitlab.com/cgs-neurons/bilconv/-/archive/master/bilconv-master.zip
```

Alternatively:
```
git clone https://gitlab.com/cgs-neurons/bilconv.git
cd bilconv
```

### Adjust source and output paths, dataset parameters

Edit ./config.env as appropriate.
The source path can be a single directory for the input tif files OR a comma separated list if you want the
dataset to contain multiple channels. Each channel is derived from each directory.
(IMPORTANT: the output directory MUST end in .omehans, the script itself uses this as a hidden flag as well as the BIL server when you upload to the staging area):

* `SOURCE_PATH=/path/to/source/tif/files, /additional/directories/for/channels, ...`
* `OUTPUT_PATH=/path/to/output/my_dataset.ome.zarr.omehans`
* `CONV_ARGS` are any arguments accepted by stack_to_multiscale_ngff. For details, see https://github.com/CBI-PITT/stack_to_multiscale_ngff#stack_to_multiscale_ngff

Note that the misspelling of "original" is intentional. Also the number of channel labels should match the
number of source_path input folders
```
CONV_ARGS=\
--name my_dataset \
--channelLabels Channel_0 Channel_1 ... \
--scale 1 1 0.280 0.114 0.114 \
--origionalChunkSize 1 1 64 64 64 \
--finalChunkSize 1 1 64 64 64 \
--fileType tif
```

### Update image
If a change was made to the conversion project, run this script to get the latest image.

```
./update-image.sh
```

### Run image conversion

```
./run.sh
```

## (Developers) Build

### Clone the main repository

The main code resides in an external repository. Run in this directory:
```
git clone https://github.com/CBI-PITT/stack_to_multiscale_ngff.git
```

### Build the docker image

Make sure the DockerFile is at the same level as the stack_to_multiscale_ngff directory that has the external repo code.
```
docker build -t registry.gitlab.com/cgs-neurons/bilconv .
```

### Push the image to gitlab

Log into the container registry:
```
docker login registry.gitlab.com -u USERNAME -p PERSONAL_ACCESS_TOKEN
```

To create a personal access token, visit:
https://gitlab.com/-/profile/personal_access_tokens and add a token that grants the `api`, `read_registry`, `write_registry` permissions.


The command will push out the image tagged with latest to the gitlab container registry.
```
docker push registry.gitlab.com/cgs-neurons/bilconv
```