# FROM condaforge/mambaforge:4.9.2-5 AS conda
FROM python:3.8-slim

WORKDIR /srv
COPY ./stack_to_multiscale_ngff/ .
RUN apt update
RUN apt install git -y
RUN python3 -m pip install .
# Default build.py parameters
ENV CONV_ARGS="--scale 1 1 0.280 0.114 0.114 \
  --origionalChunkSize 1 1 64 64 64 \
  --finalChunkSize 1 1 64 64 64 \
  --fileType tif"
# Docker container input location pass in as param
# Can be a single path or multiple ones separated by space
ENV DOCKER_IN_LOCATIONS="/data/input"
CMD python /srv/stack_to_multiscale_ngff/builder.py ${DOCKER_IN_LOCATIONS} /data/output.omehans ${CONV_ARGS}